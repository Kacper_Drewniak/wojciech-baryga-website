module.exports = {
    siteMetadata: {
        siteUrl: "https://www.yourdomain.tld",
        title: "My Gatsby Site",
    },
    plugins: [
        "gatsby-plugin-sass",
        "gatsby-plugin-react-helmet",
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                icon: `src/images/icon.png`,
                name: `Wojciech Baryga`,
                short_name: `Wojciech Baryga`,
                start_url: `/`,
                background_color: `#f7f0eb`,
                theme_color: `#ffffff`,
                display: `standalone`,
            },
        },
    ],
};
